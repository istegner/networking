Command line:
drawserver [port value] [res width height]

Examples:
drawserver port 1313
drawserver res 512 512
drawserver port 1345 res 256 256

The default port number is 1300.
The default resolution is 512 x 512.
