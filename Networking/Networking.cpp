#include <iostream>
//Set up Winsock
#include <WinSock2.h>
#include <MSWSock.h>
#include <WS2tcpip.h>
#include "packet.h"

#pragma comment(lib, "ws2_32.lib")

SOCKET CreateSocket();
int RecievePacket(SOCKET socket);

template<typename T> 
int SendPacket(T toSend, SOCKET socket, sockaddr_in address)
{
	int result;
	result = sendto(socket, (const char*)&toSend, sizeof(T), 0, (SOCKADDR*)& address, sizeof(address));
	if (result == SOCKET_ERROR)
	{
		std::cout << "sendto() failed: Error " << WSAGetLastError() << std::endl;
		return result;
	}
}

int main(int argc, char* argv[])
{
	std::string ipAddress;
	int port;
	if (argc == 3)
	{
		ipAddress = argv[1];
		port = atoi(argv[2]);
	}
	else
	{
		/*ipAddress = "127.0.0.1";
		port = 1300;*/
		std::cout << "IPAddress: ";
		std::cin >> ipAddress;
		std::cout << "Port: ";
		std::cin >> port;
	}


	//Init Winsock
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		std::cout << "Failed to initialise winsock" << std::endl;
		return 1;
	}

	//Make an address for sending
	sockaddr_in address;
	//IPv4 address family
	address.sin_family = AF_INET;
	//Packets will be sent to ipAddress
	InetPton(AF_INET, ipAddress.c_str(), &address.sin_addr.s_addr);
	//Port to listen to
	address.sin_port = htons(port);

	//Create a socket
	SOCKET mySocket = CreateSocket();
	if (mySocket == NULL)
	{
		std::cout << "Failed to open socket" << std::endl;
		return 1;
	}

	//Announce client to server
	PacketClientAnnounce announceMessage;
	std::string name;
	std::cout << "Client name: ";
	std::cin >> name;
	strcpy_s(announceMessage.name, 50, name.c_str());
	SendPacket(announceMessage, mySocket, address);
	while (!RecievePacket(mySocket));

	//Send packet
	PacketBox message;
	message.x = 0;
	message.y = 0;
	message.w = 100;
	message.h = 100;
	message.r = 0;
	message.g = 0;
	message.b = 1;
	SendPacket(message, mySocket, address);

	//Recieve packet
	RecievePacket(mySocket);

	//Cleanup
	WSACleanup();

	return 0;
}

SOCKET CreateSocket()
{
	SOCKET mySocket = socket(AF_INET, SOCK_DGRAM, 0);
	if (mySocket == SOCKET_ERROR)
	{
		std::cout << "Error opening socket" << std::endl;
		return NULL;
	}

	//Fix for socket breaking if receiver crashed
	DWORD dwBytesReturned = 0;
	BOOL bNewBehaviour = FALSE;
	WSAIoctl(mySocket,
		SIO_UDP_CONNRESET,
		&bNewBehaviour,
		sizeof(bNewBehaviour),
		NULL, 0,
		&dwBytesReturned,
		NULL, NULL);

	//Allow broadcasting
	BOOL bOptVal = TRUE;
	setsockopt(mySocket,
		SOL_SOCKET,
		SO_BROADCAST,
		(const char*)& bOptVal,
		sizeof(BOOL));

	return mySocket;
}

/// <summary>
/// 
/// </summary>
/// <param name="message"></param>
int RecievePacket(SOCKET socket)
{
	int waiting;
	do
	{
		fd_set checksockets;
		checksockets.fd_count = 1;
		checksockets.fd_array[0] = socket;

		struct timeval t;
		t.tv_sec = 0;
		t.tv_usec = 0;

		waiting = select(NULL, &checksockets, NULL, NULL, &t);

		if (waiting > 0)
		{
			char buffer[10000];
			Packet* p;
			sockaddr_in from;
			int fromLength = sizeof(from);
			int size = recvfrom(socket, buffer, 10000, 0, (SOCKADDR*)& from, &fromLength);
			p = (Packet*)buffer;
			switch (p->type)
			{
			case Packet::e_serverInfo:
			{
				PacketServerInfo* pa = (PacketServerInfo*)p;
				std::cout << "Width: " << pa->width << " Height: " << pa->height << std::endl;
				return 1;
				break;
			}
			}
		}
	} while (waiting);

	return 0;
}
